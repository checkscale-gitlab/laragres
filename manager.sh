#!/bin/bash

VIO='\033[0;35m'
RED='\033[0;31m'
NC='\033[0m'


menu () {
  echo -e "Modo de empleo: ./manager.sh [build|clean|start|stop|status]\n"
  echo -e "  build \t Contruir las imagenes y el entorno"
  echo -e "  clean \t Limpiar el entorno y eliminar los volumenes"
  echo -e "  start \t Iniciar el contenedor y la aplicacion"
  echo -e "  stop  \t Detener el contenedor y la aplicacion"
  echo -e "  status  \t Obtener el estado de la aplicacion"
  echo -e "\n"
}


# Comprobando los argumentos.
if [ $# -ne 1 ]; then
  menu
  exit 1
fi

# Comprobando la tarea a ejecutar.
case "$1" in
  build)
    echo -e "\n${VIO}[*] Instalando Dependencias de la Aplicación...${NC}\n"
    php artisan key:generate --ansi 
    composer install
    echo -e "\n${VIO}[*] Construyendo la Aplicación...${NC}\n"
    docker-compose build
    echo -e "\n${VIO}[*] Iniciando la Aplicación...${NC}\n"
    docker-compose up -d
    echo -e "\n${VIO}[*] Realizando las Migraciones de la Base de Datos...${NC}\n"
    docker-compose exec app php ./var/www/html/artisan migrate
    echo -e "\n${VIO}[*] Corriendo los Seeders para la Base de Datos...${NC}\n"
    docker-compose exec app php ./var/www/html/artisan db:seed
    ;;

  clean)
    echo -e "\n${VIO}[*] Limpiando la Aplicación...${NC}\n"
    docker-compose down -v
    ;;
    
  stop)
    echo -e "\n${VIO}[*] Deteniendo la Aplicación...${NC}\n"
    docker-compose stop
    ;;

  start)
    echo -e "\n${VIO}[*] Iniciando la Aplicación...${NC}\n"
    docker-compose up -d
    ;;

  status)
    docker-compose ps 
    ;;

  *)
    echo -e "${RED}manager.sh: opción inválida -- '$1'${NC}\n"
    menu
    exit 1
 
esac
