# laragres

A Laravel Plataform using Postgresql and Docker.


## Requirements:

- Docker
- Docker-compose
- Composer
- PHP


## How to use:

1. Use credentials example of "docker-compose.yml" file and set same data (you can change it) into .env file (if not exists, do: `cp .env.example .env`)
2. Simply run: `manager.sh build`, to build environment an start application.